import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GatewayController } from './gateways/gateway.controller';
import { GatewayService } from './gateways/gateway.service';
import { Gateway, GatewaySchema } from './gateways/schemas/gateway.schema';
import { Device, DeviceSchema } from './devices/schemas/device.schema';
import { DeviceController } from './devices/device.controller';
import { DeviceService } from './devices/device.service';

@Module({
  providers: [GatewayService,DeviceService],
  controllers: [GatewayController,DeviceController],
  imports: [
    MongooseModule.forFeature([{ name: Gateway.name, schema: GatewaySchema },{ name: Device.name, schema: DeviceSchema }]),
  ],
})
export class GatewayManagmentModule {}
