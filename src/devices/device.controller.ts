import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateDeviceDto } from './dto/create-device.dto';
import { UpdateDeviceDto } from './dto/update-device.dto';
import { DeviceService } from './device.service';

@Controller('devices')
export class DeviceController {
  constructor(private readonly service: DeviceService) {}

  @Get(':gatewayId')
  async index(@Param('gatewayId') gatewayId: string) {
    return await this.service.findAllById(gatewayId);
  }

  @Post()
  async create(@Body() createDeviceDto: CreateDeviceDto) {
    console.log({ createDeviceDto });
    return await this.service.create(createDeviceDto);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updateDeviceDto: UpdateDeviceDto) {
    return await this.service.update(id, updateDeviceDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.service.delete(id);
  }
}
