import { BaseDeviceDto } from './base-device.dto';

export class UpdateDeviceDto extends BaseDeviceDto {
  completedAt: Date;
}
