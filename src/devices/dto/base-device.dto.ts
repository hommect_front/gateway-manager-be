export class BaseDeviceDto {
  UID: string;
  vendor?: string;
  status?: boolean;
  gatewayId?: string;
}
