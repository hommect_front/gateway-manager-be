import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type DeviceDocument = Device & Document;

@Schema()
export class Device {
  @Prop({ required: true })
  UID: string;

  @Prop()
  vendor?: string;

  @Prop()
  status?: boolean;

  @Prop()
  gatewayId?: string;

  @Prop()
  completedAt?: Date;

  @Prop()
  createdAt: Date;

  @Prop()
  deletedAt?: Date;
}

export const DeviceSchema = SchemaFactory.createForClass(Device);
