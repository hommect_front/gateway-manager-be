import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateDeviceDto } from './dto/create-device.dto';
import { UpdateDeviceDto } from './dto/update-device.dto';
import { Device, DeviceDocument } from './schemas/device.schema';

@Injectable()
export class DeviceService {
  constructor(
    @InjectModel(Device.name) private readonly model: Model<DeviceDocument>,
  ) {}

  async findAll(): Promise<Device[]> {
    return await this.model.find().exec();
  }

  async findAllById(gatewayId): Promise<Device[]> {
    return await this.model.find({ gatewayId: gatewayId }).exec();
  }

  async findOne(id: string): Promise<Device> {
    return await this.model.findById(id).exec();
  }

  async create(createDeviceDto: CreateDeviceDto): Promise<any>  {
    const devices = await this.model.find({ gatewayId: createDeviceDto.gatewayId }).exec();
    if(devices.length >= 3){
      return new Promise<string>((resolve, reject) => {
        resolve('You exceed the limit of devices for single gateway');
      })
    }
    return await new this.model({
      ...createDeviceDto,
      createdAt: new Date(),
    }).save();
  }

  async update(id: string, updateDeviceDto: UpdateDeviceDto): Promise<Device> {
    return await this.model.findByIdAndUpdate(id, updateDeviceDto).exec();
  }

  async delete(id: string): Promise<Device> {
    return await this.model.findByIdAndDelete(id).exec();
  }
}
