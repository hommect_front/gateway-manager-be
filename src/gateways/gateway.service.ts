import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateGatewayDto } from './dto/create-gateway.dto';
import { UpdateGatewayDto } from './dto/update-gateway.dto';
import { Gateway, GatewayDocument } from './schemas/gateway.schema';

@Injectable()
export class GatewayService {
  constructor(
    @InjectModel(Gateway.name) private readonly model: Model<GatewayDocument>,
  ) {}

  async findAll(): Promise<Gateway[]> {
    return await this.model.find().exec();
  }

  async findOne(id: string): Promise<Gateway> {
    return await this.model.findById(id).exec();
  }

  async create(createGatewayDto: CreateGatewayDto): Promise<Gateway> {
    return await new this.model({
      ...createGatewayDto,
      createdAt: new Date(),
    }).save();
  }

  async update(id: string, updateGatewayDto: UpdateGatewayDto): Promise<Gateway> {
    return await this.model.findByIdAndUpdate(id, updateGatewayDto).exec();
  }

  async delete(id: string): Promise<Gateway> {
    return await this.model.findByIdAndDelete(id).exec();
  }
}
