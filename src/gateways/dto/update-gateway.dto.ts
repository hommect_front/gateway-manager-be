import { BaseGatewayDto } from './base-gateway.dto';

export class UpdateGatewayDto extends BaseGatewayDto {
  completedAt: Date;
}
