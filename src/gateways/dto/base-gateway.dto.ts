export class BaseGatewayDto {
  serialNumber: string
  humanName?: string;
  ipAddress?: string;
}
