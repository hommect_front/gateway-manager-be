import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type GatewayDocument = Gateway & Document;

@Schema()
export class Gateway {
  @Prop({ required: true })
  serialNumber: string;

  @Prop()
  humanName?: string;

  @Prop()
  ipAddress?: string;

  @Prop()
  completedAt?: Date;

  @Prop()
  createdAt: Date;

  @Prop()
  deletedAt?: Date;
}

export const GatewaySchema = SchemaFactory.createForClass(Gateway);
