import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateGatewayDto } from './dto/create-gateway.dto';
import { UpdateGatewayDto } from './dto/update-gateway.dto';
import { GatewayService } from './gateway.service';

@Controller('gateways')
export class GatewayController {
  constructor(private readonly service: GatewayService) {}

  @Get()
  async index() {
    return await this.service.findAll();
  }

  @Get(':id')
  async find(@Param('id') id: string) {
    return await this.service.findOne(id);
  }

  @Post()
  async create(@Body() createGatewayDto: CreateGatewayDto) {
    console.log({ createGatewayDto });
    return await this.service.create(createGatewayDto);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updateGatewayDto: UpdateGatewayDto) {
    return await this.service.update(id, updateGatewayDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.service.delete(id);
  }
}
