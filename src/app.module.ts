import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GatewayModule } from './gateways/gateway.module';
import { Gateway, GatewaySchema } from './gateways/schemas/gateway.schema';
import { Device, DeviceSchema } from './devices/schemas/device.schema';
import { GatewayManagmentModule } from './gatewayManagment.module';

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost/gateway_management'), GatewayManagmentModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
